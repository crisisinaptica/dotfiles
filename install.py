#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import getenv, listdir, getcwd
from os.path import isdir
from subprocess import run
import argparse


parser = argparse.ArgumentParser(description='Dotfiles management script.')
parser.add_argument('operation', choices=['link', 'delete', 'relink'])
parser.add_argument(dest='packages', nargs='*', default=None)
args = parser.parse_args()

operation = args.operation
cmd_packages = args.packages

all_packages = [p for p in listdir(getcwd())
                if isdir(p) and not p.startswith('.')]


def stow(command, pkg):

    commands = {
        'link':   {'flag': '-S', 'msg': 'Linked '},
        'delete': {'flag': '-D', 'msg': 'Deleted '},
        'relink': {'flag': '-R', 'msg': 'Relinked '}
    }

    run(['stow', '-t', getenv('HOME'), commands[command]['flag'], pkg])
    print('INFO: ' + commands[command]['msg'] + 'package \'' + pkg + '\'')
    return


def main():

    if cmd_packages:
        for package in cmd_packages:
            if package not in all_packages:
                print('WARNING: Could not find \"' + package + '\" package.\n'
                      '         Skipping!')
                continue
            else:
                stow(operation, package)

    else:
        for package in all_packages:
            stow(operation, package)

if __name__ == '__main__':
    main()
