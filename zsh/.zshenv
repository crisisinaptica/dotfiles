#@IgnoreInspection BashAddShebang

export EDITOR=vim
export SUDO_EDITOR=sudoedit
export PAGER=less

export GEM_HOME=$(ruby -e 'print Gem.user_dir')
export NPM_PACKAGES="${HOME}/.npm_packages"
export MOZILLA_CERTIFICATE_FOLDER="${HOME}/.mozilla/firefox/x39d74ub.default"

if [ -f /usr/bin/blender ]; then
	blender_version=$(blender -v | sed -rn '/^Blender/p' | cut -d' ' -f2)
	export OCIO="/usr/share/blender/${blender_version}/datafiles/colormanagement/config.ocio"
	export PYTHONPATH="/usr/share/blender/${blender_version}/scripts/modules/:${PYTHONPATH}"
fi

if [ -f /usr/bin/gimp ]; then
	gimp_version=$(gimp --version | awk '{print $NF}' | cut -d'.' -f 1,2)
	export PYTHONPATH="$HOME/.config/GIMP/${gimp_version}/plug-ins/:${PYTHONPATH}"
fi

### PATH

typeset -U path
path=(${GEM_HOME}/bin ${HOME}/.npm-packages/bin
/usr/local/bin ${HOME}/scripts/mantenimiento ${HOME}/scripts/ejecutables
${HOME}/.local/bin ${HOME}/.local/bin/INTERFACE $path[@])
