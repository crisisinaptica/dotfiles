# dotfiles #

Local files are managed with [Gnu Stow](https://www.gnu.org/software/stow/) as described in this [cool article by Brandon Invergo](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html).

A very practical way for keeping all your dotfiles organized and versioned.

I've also written a simple script to manage all the packages at once:
```
usage: install.py [-h] {link,delete,relink} [packages [packages ...]]
```

Specify an action (link, delete, relink) and it will be executed for all the packages passed as arguments.
If no package name is given, it will act upon all the ones present in the current working directory.
